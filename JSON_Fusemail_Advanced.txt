{
    "dataLists": [
        {
            "content": [
                "gz",
                "hqx",
                "arj",
                "cab",
                "rar",
                "msi",
                "gzip",
                "pkzip",
                "lha",
                "zip",
                "7z",
                "ace",
                "jar",
                "dll",
                "exe",
                "com",
                "scr",
                "pif",
                "bat",
                "ocx"
            ],
            "name": "File type blocking ottodanielsen.com"
        }
    ],
    "legacyCompanyId": 1,
    "legacyId": "5816",
    "legacyType": "customer",
    "policies": [
        {
            "actions": [
                {
                    "contents": {
                        "days": 0,
                        "maxDownloads": 0,
                        "replacementHTML": "<p>The attachment <span class=\"error\">[%attach_name%]</span> was deleted as it was not compliant with the internal IT policy.</p>"
                    },
                    "type": "detach_attachment"
                }
            ],
            "enabled": true,
            "mailDirection": "inbound",
            "name": "File type blocking ottodanielsen.com",
            "rules": {
                "entries": [
                    {
                        "comparator": "in_list",
                        "property": "Attachment Ext",
                        "propertyKey": "ATTACHMENT_NAME",
                        "value": "File type blocking ottodanielsen.com",
                        "valueType": "string"
                    }
                ],
                "type": "and"
            },
            "scope": {
                "constraint": "specified",
                "entries": [
                    "ottodanielsen.com"
                ],
                "type": "domain"
            }
        }
    ],
    "quarantineReport": {
        "filters": {
            "quarantineSpf": "hard"
        },
        "standard": {
            "allowTimeChange": true,
            "autoLoginURL": true,
            "content": {
                "bulk": {
                    "allow": false,
                    "deny": false,
                    "enabled": false,
                    "release": true,
                    "show": true
                },
                "contents": {
                    "enabled": true,
                    "release": true,
                    "show": true
                },
                "spam": {
                    "allow": false,
                    "deny": false,
                    "enabled": true,
                    "release": true,
                    "show": true
                },
                "virus": {
                    "enabled": true,
                    "show": true
                }
            },
            "deliveryDays": [
                1,
                2,
                3,
                4,
                5,
                6,
                7
            ],
            "deliveryHours": [
                1
            ],
            "enabled": true,
            "maxSpamThreatLevel": 2
        }
    },
    "statisticsReports": [
        {
            "content": {
                "parameters": [
                    {
                        "name": "top",
                        "value": "20"
                    }
                ],
                "period": "Last 7 Days"
            },
            "enabled": true,
            "recipients": [
                {
                    "email": "od@ottodanielsen.com",
                    "name": "PDF report recipient"
                }
            ],
            "scheduleDetails": {
                "deliveryHours": [
                    5
                ],
                "frequency": "Daily"
            },
            "title": "E-mail trafik rapport"
        }
    ]
}
{
  "type": "object",
  "properties": 
  {
    "name": 
    {
      "type": "string",
      "required": true,
      "description": "A human-readable description of this policy"
    },
    "mail_direction": 
    {
      "type": "string",
      "required": true,
      "enum": ["inbound", "outbound"],
      "description": "The type of mail this policy will be applied to"
    },
    "enabled": 
    {
      "type": "boolean",
      "default": true,
      "description": "Is this policy enabled."
    },
    "notes": 
    {
      "description": "Further description of this policy",
      "type": "string"
    },
    "scope": 
    {
      "type": "object",
      "description": "Which domains or mailboxes this policy will apply to",
      "properties": 
      {
        "type": 
        {
          "type": "string",
          "enum": ["domain", "mailbox"],
          "required": true,
          "description": "If this policy used the 'except' or 'specified' constraint, then this describes the type of data in the 'entries' element."
        },
        "constraint": 
        {
          "type": "string",
          "required": true,
          "enum": ["all", "specified", "except"],
          "description": "The kind of scope specification to be applied"
        },
        "entries": 
        {
          "type": "array",
          "description": "If this policy used the 'except' or 'specified' constraint, then this contains a list of mailboxes or domains to be excluded or included for policy application. Required for 'except' and 'specified' constarints.",
          "items": 
          {
            "type": "string",
            "description": "One mailbox or domain name"
          }
        }
      }
    },
    "rules": 
    {
      "description": "The set of rules used to determine if this policy will be applied to a message.",
      "type": "object",
      "properties": 
      {
        "type": 
        {
          "type": "string",
          "enum": ["any", "all", "points_greater_than", "points_less_than", "always"],
          "required": true,
          "description": "The logical operation used to handle multiple rules."
        },
        "points": 
        {
          "type": "integer",
          "description": "Some policies calculate a numerical score for a message. If the 'points_greater_than' or 'points_less_than' type is used, then the total points assigned by rules will be compared to this number to determine if the policy matches. Required for these types."
        },
        "entries": 
        {
          "description": "The rules used to match messages.",
          "type": "array",
          "items": 
          {
            "type": "object",
            "properties": 
            {
              "property": 
              {
                "type": "string",
                "required": true,
                "description": "The property of the message to be matched. For example, 'Subject' would match the Subject header, or 'Attachment Ext' would match the file extension of any attachment."
              },
              "comparator": 
              {
                "type": "string",
                "required": true,
                "description": "The operation used to compare 'property' to 'value'. For example, 'IN_LIST' tests if the value of of the message's 'property' is in the list specified in 'value'"
              },
              "value": 
              {
                "type": "string",
                "required": true,
                "description": "The value 'property' will be compared to"
              },
              "value_type": 
              {
                "type": "string",
                "enum": ["domain","mailbox","ip","text","list","int","string"],
                "required": true,
                "description": "The type of data stored in 'value'"
              },
              "occurrence_option": 
              {
                "type": "string",
                "enum": ["exactly", "at_least", "at_most"],
                "default": "at_least",
                "description": "Some rules may look for something that can occur more than once in a message. In these cases, occurrence_option specifies how the number of times an event occurs in a message will be compared to occurence. For example, 'occurrence_option'='AT_LEAST' and 'occurrence'=5 would match if the rule would match 5 or more different places in a message."
              },
              "occurrence": 
              {
                "type": "integer",
                "default": 1,
                "description": "Some rules may look for an event that can occur more than once in a message. In these cases, occurrence specifies a number of times an event must occur in a message."
              },
              "assigned_points": 
              {
                "type": "integer",
                "description": "Some policies calculate a numerical score for a message. This allows you to assign a number of points if this rule matches. Required for 'points_greater_than' and 'points_less_than' list types."
              }
            }
          }
        }
      }
    },
    "actions": 
    {
      "type": "array",
      "items": 
      {
        "type": "object",
        "properties": 
        {
          "type": 
          {
            "type": "string",
            "required": true,
            "description": "The type of action to be performed. For example 'add_html_footer' adds a specified footer to HTML messages"
          },
          "position": 
          {
            "type": "integer",
            "minimum": 1,
            "description": "Describes the execution priority of the action in this list. Lower positions will be executes first. If no positions are specified for the list, positions will be assigned in the order of the list."
          },
          "contents": 
          {
            "type": "object",
            "description": "Contains data specific to the type of the action."
          }
        }
      }
    }
  }
}
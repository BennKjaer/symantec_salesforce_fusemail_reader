using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace Symantec_Reader
{
    /// <summary>
    /// Summary description for msSqlConnectionInterface
    /// </summary>
    class msSqlConnectionInterface
    {
        private SqlConnection msSqlConnection = new SqlConnection();
        private int maxTimeOut = 600;

        // Connectionstring
        public string ConnectionString
        {
            get
            {
                return msSqlConnection.ConnectionString;
            }
            set
            {
                msSqlConnection.ConnectionString = value;
            }
        }

        /// <summary>
        /// �bner en forbindelse til en MySQL server
        /// </summary>
        /// <param name="connectionString">ConnectionString</param>
        /// <returns>MySqlConnection object</returns>
        public void OpenSQLConnection()
        {
            // Forbind til MySQL serveren
            try
            {
                // Fors�g at �bne, hvis der er 
                if (msSqlConnection.ConnectionString != null)
                {
                    msSqlConnection.Open();
//                    this.ExecuteSQLQueryNoReturn( "SET SESSION wait_timeout = " + maxTimeOut );
                }
                else
                {
                    // Der er ikke angivet en connectionstring
                    throw new ArgumentException(@"No connectionstring specified");
                }
            }
            catch ( SqlException ex )
            {
                // Fejl, tyr den exception
                throw new ArgumentException(@"Could not open MySQL connection: "+ex.ToString());
            }
        }

        /// <summary>
        /// Lukker MySQLforbindelsen
        /// </summary>
        public void CloseMsSQLConnection()
        {
            msSqlConnection.Close();
        }

        /// <summary>
        /// Udf�rer en SQL kommando, og returnerer en DataReader
        /// </summary>
        /// <param name="SQLCommand">SQL string</param>
        /// <returns>DataReader</returns>
        public DataTable ExecuteSQLQueryMultiple( string SQLCommand )
        {
            try
            {
                // Check om forbindelsen er �ben, og hvis ikke, �ben den
                if( msSqlConnection.State != ConnectionState.Open )
                {
                    this.OpenSQLConnection();
                }

                // Udf�r SQL query, over i en DataTable
                SqlDataAdapter msSqlDataAdapter = new SqlDataAdapter( SQLCommand, msSqlConnection );
                msSqlDataAdapter.SelectCommand.CommandTimeout = maxTimeOut;

                DataTable returnDataTable = new DataTable();

                msSqlDataAdapter.Fill( returnDataTable );

                return returnDataTable;
            }
            catch( Exception ex )
            {
                // Fejl, tyr den exception
                throw new ArgumentException( @"Could not execute 'ExecuteSQLQueryMultiple': " + ex.ToString() );
            }
        }

        /// <summary>
        /// Udf�rer en SQL kommando, og returnerer et objekt
        /// </summary>
        /// <param name="SQLCommand">SQL string</param>
        /// <returns>Object</returns>
        public Object ExecuteSQLQuerySingle(string SQLCommand)
        {
            try
            {
                // Check om forbindelsen er �ben, og hvis ikke, �ben den
                if( msSqlConnection.State != ConnectionState.Open )
                {
                    this.OpenSQLConnection();
                }

                // Udf�r SQL query
                SqlCommand mySQLCommand = new SqlCommand();
                mySQLCommand.CommandTimeout = maxTimeOut;  // 30 minutters ventetid
                mySQLCommand.Connection = msSqlConnection;
                mySQLCommand.CommandText = SQLCommand;

                return mySQLCommand.ExecuteScalar();
            }
            catch( Exception ex )
            {
                // Fejl, tyr den exception
                throw new ArgumentException( @"Could not execute 'ExecuteSQLQuerySingle': " + ex.ToString() );
            }

        }

        /// <summary>
        /// Udf�rer en SQL kommando, som ikke returnerer en v�rdi
        /// </summary>
        /// <param name="SQLCommand">SQL string</param>
        public void ExecuteMsSQLQueryNoReturn(string SQLCommand)
        {
            try
            {
                // Check om forbindelsen er �ben, og hvis ikke, �ben den
                if( msSqlConnection.State != ConnectionState.Open )
                {
                    this.OpenSQLConnection();
                }

                // Udf�r SQL query
                SqlCommand mySQLCommand = new SqlCommand();
                mySQLCommand.CommandTimeout = maxTimeOut;  // 30 minutters ventetid
                mySQLCommand.Connection = msSqlConnection;
                mySQLCommand.CommandText = SQLCommand;

                mySQLCommand.ExecuteNonQuery();
            }
            catch( Exception ex )
            {
                // Fejl, tyr den exception
                throw new ArgumentException( @"Could not execute 'ExecuteSQLQueryNoReturn': " + ex.ToString() );
            }

        }

        /// <summary>
        /// Escaper farlige tegn ud af en string, til inds�ttelse i SQL
        /// </summary>
        /// <param name="SQLCommand">string</param>
        /// <returns>string</returns>
        public string EscapeSQLString( string SQLCommand )
        {
            /* Escaper mySQL-farlige chars i en tekststreng

             \0    An ASCII 0 (NUL) character.
             \'    A single quote (`'') character.
             \"    A double quote (`"') character.
             \n    A newline (linefeed) character.
             \r    A carriage return character.
             \t    A tab character.
             \\    A backslash (`\') character.
             \%    A `%' character. See note following table.
             \_    A `_' character. See note following table. */

            SQLCommand = SQLCommand.Replace( @"\", @"\\" );
            SQLCommand = SQLCommand.Replace( "\x0", "\0" );
            SQLCommand = SQLCommand.Replace( @"'", @"\'" );
            SQLCommand = SQLCommand.Replace( @"""", @"\""" );
            SQLCommand = SQLCommand.Replace( "\x10", @"\n" );
            SQLCommand = SQLCommand.Replace( "\x13", @"\r" );
            SQLCommand = SQLCommand.Replace( "\x9", @"\t" );

            return SQLCommand;
        }
    }
}
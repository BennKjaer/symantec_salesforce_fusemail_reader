﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Configuration;
using System.Data;
using System.IO;
using System.Net;
using System.ServiceModel.Channels;

using Microsoft.Web.Services2;
using Microsoft.Web.Services2.Security;
using Microsoft.Web.Services2.Security.Tokens;

namespace Symantec_Reader
{

    /// <summary>
    /// This Reader will generate two different JSON files, one for SalesForce and one for FuseMail.
    /// </summary>

    class Program
    {
        public static ConfigService service = null;    // = new ConfigService();

        static string version = Assembly.GetEntryAssembly().GetName().Version.ToString();
        static string application_name = "Symantec Migration";
        static string checklog = "Please check the log file for detailed information";
        static string logfilepath = @"Log files is placed under \Logs\<current data>.txt";
        static string masterFilenameSalesForce = "SymantecJSONSalesForce.txt";
        static string masterFilenameFuseMailBasic = "SymantecJSONFuseMailBasic.txt";
        static string masterFilenameFuseMailAdv = "SymantecJSONFuseMailAdvanced.txt";
        static string masterFilenameFuseMailBasicMS = "SymantecJSONFuseMailBasicMS.txt";
        static string masterFilenameFuseMailAdvMS = "SymantecJSONFuseMailAdvancedMS.txt";
        static string datetimeStamp = DateTime.Now.ToString( "yyyyMMdd_HHmm" );
        static int timesToRetryEstablishConnection = 1;

        static string username = string.Empty;
        static string password = string.Empty;

        public static string connectionStringMSSQL = string.Empty;
        public static string connectionStringMYSQL = string.Empty;
        public static bool overwriteLogfile = true;

        public static string[] customerServices = null;

        // Create Sql interfaces
        public static mySqlConnectionInterface MySqlInterface;
        public static msSqlConnectionInterface MsSqlInterface;

        public static DataTable dtCustomer = new DataTable();
        public static DataTable dtLDAPCustomer = new DataTable();
        public static DataTable dtEPACustomer = new DataTable();
        public static DataTable dtCustomerDomains = new DataTable();
        public static DataTable dtDomainSpamService = new DataTable();

        public static DataRow[] customerRow = null;

        public static StringBuilder SalesForceJsonString = new StringBuilder();
        public static StringBuilder FuseMailBasicJsonString = new StringBuilder();
        public static StringBuilder FuseMailAdvJsonString = new StringBuilder();
        public static StringBuilder FuseMailBasicMSJsonString = new StringBuilder();
        public static StringBuilder FuseMailAdvMSJsonString = new StringBuilder();

        static void Main( string[] args )
        {
            // Create subdirectories for output
            CreateOutputPath();

            // Read settings
            ReadSetup();

            // Create connection to the MySQL database
            if( !CreateMYSQLCommection() )
            {
                ExitApplication( "ERROR - MySQL connection failed. Check that VPN is connected. application stopped." + Environment.NewLine + checklog + Environment.NewLine + logfilepath );
            }

            // Create connection to the MSSQL database
            if( !CreateMSSQLCommection() )
            {
                ExitApplication( "ERROR - MS-SQL connection failed. Check that VPN is connected. application stopped." + Environment.NewLine + checklog + Environment.NewLine + logfilepath );
            }

            // Configurate service instance
            service = SymantecApi.ConfigServiceInstance( username, password );


            // Define DataTables
            DefineDataTables();

            // Select data from SQL databases
            SelectMsSqlData();
            SelectMySqlData();

            GenerateOutput();

//            SelectAPIdata();

        }

        /// <summary>
        /// CreateOutputPath()
        /// </summary>
        private static void CreateOutputPath()
        {
            try
            {
                string subDirOutput = Path.GetDirectoryName( Assembly.GetExecutingAssembly().Location ).ToString() + @"\Output";
                string subDirLogs = Path.GetDirectoryName( Assembly.GetExecutingAssembly().Location ).ToString() + @"\Logs";

                if( !Directory.Exists( subDirOutput ) )
                {
                    Directory.CreateDirectory( subDirOutput );
                }

                if( !Directory.Exists( subDirLogs ) )
                {
                    Directory.CreateDirectory( subDirLogs );
                }
            }
            catch( Exception ex )
            {
                ExitApplication( "ERROR - Creating subdirectories., application stopped." + Environment.NewLine + checklog + Environment.NewLine + ex.ToString() );
            }
        }

        /// <summary>
        /// SelectMsSqlData()
        /// </summary>
        static private void SelectMsSqlData()
        {
            Console.WriteLine( "SelectMsSqlData()" );

            // Select Partner data
            if( SelectCustomerData() )
            {
                WriteToLog( "SelectCustomerData() - ok", false );
            }
            else
            {
                ExitApplication( "ERROR - Failed to retrive Customer data from MsSQL" );
            }

            // Select EPA customers
            if( SelectEPACustomers() )
            {
                WriteToLog( "SelectEPACustomers() - ok", false );
            }
            else
            {
                ExitApplication( "ERROR - Failed to retrive EPA Customer data from MySQL" );
            }

        }

        /// <summary>
        /// SelectMySqlData()
        /// </summary>
        static private void SelectMySqlData()
        {
            Console.WriteLine( "SelectMySqlData()" );

            // Select Partner data
            if( SelectLDAPCustomers() )
            {
                WriteToLog( "SelectLDAPCustomers() - ok", false );
            }
            else
            {
                ExitApplication( "ERROR - Failed to retrive LDAP Customer data from MySQL" );
            }

        }

        /// <summary>
        /// DefineDataTables()
        /// </summary>
        static private void DefineDataTables()
        {
            dtCustomerDomains.Columns.Add( "CustomerId", typeof( int ) );
            dtCustomerDomains.Columns.Add( "Id", typeof( int ) );
            dtCustomerDomains.Columns.Add( "Name", typeof( string ) );
            dtCustomerDomains.Columns.Add( "Services", typeof( string ) );

            dtDomainSpamService.Columns.Add( "CustomerId", typeof( int ) );
            dtDomainSpamService.Columns.Add( "DomainId", typeof( int ) );
            dtDomainSpamService.Columns.Add( "UseDomainDefaultSettings", typeof( bool ) );
            dtDomainSpamService.Columns.Add( "UseCustomIPAllowedList", typeof( bool ) );
            dtDomainSpamService.Columns.Add( "UseCustomDomainAllowedList", typeof( bool ) );
            dtDomainSpamService.Columns.Add( "BulkMailAddress", typeof( string ) );
            dtDomainSpamService.Columns.Add( "SubjectTag", typeof( string ) );
            dtDomainSpamService.Columns.Add( "SubjectTagPosition", typeof( string ) );
            dtDomainSpamService.Columns.Add( "HasQuarantine", typeof( bool ) );
            dtDomainSpamService.Columns.Add( "OrdbBlockedListAction", typeof( string ) );
            dtDomainSpamService.Columns.Add( "RblBlockedListAction", typeof( string ) );
            dtDomainSpamService.Columns.Add( "RssBlockedListAction", typeof( string ) );
            dtDomainSpamService.Columns.Add( "DulBlockedListAction", typeof( string ) );
            dtDomainSpamService.Columns.Add( "SigBlockedListAction", typeof( string ) );
            dtDomainSpamService.Columns.Add( "CustomIPBlockedListAction", typeof( string ) );
            dtDomainSpamService.Columns.Add( "CustomDomainBlockedListAction", typeof( string ) );
            dtDomainSpamService.Columns.Add( "HeuristicsBlockedListAction", typeof( string ) );
            dtDomainSpamService.Columns.Add( "SpfBlockedListAction", typeof( string ) );
            dtDomainSpamService.Columns.Add( "RecipientCustomListCombination", typeof( string ) );
            dtDomainSpamService.Columns.Add( "GroupCustomListCombination", typeof( string ) );
            dtDomainSpamService.Columns.Add( "RecipientCustomListMergePriority", typeof( string ) );
            dtDomainSpamService.Columns.Add( "GroupCustomListMergePriority", typeof( string ) );
            dtDomainSpamService.Columns.Add( "HasNewsLetters", typeof( bool ) );
            dtDomainSpamService.Columns.Add( "UseDMARC", typeof( bool ) );
            dtDomainSpamService.Columns.Add( "NewslettersBlockedListAction", typeof( string ) );
            dtDomainSpamService.Columns.Add( "NewslettersSubjectTag", typeof( string ) );
            dtDomainSpamService.Columns.Add( "NewslettersSubjectTagPosition", typeof( string ) );

        }

        #region MSSql

        static private bool SelectCustomerData()
        {

            Logger.Log( "SelectCustomerData()", true );

            // Get all customers with packages, licences and package groups.
            //
            // Date = curent date - 1
            // Format = '2016-10-06'
            //
            string sql = "SELECT EPAID, Id AS SymantecID, Name, Parent, SymantecCustomers.IsReseller, lookupPackages.PackageID, " +
                         "lookupPackages.PackageName, CustomerPackageHistory.Licences, lookupPackages.PackageGroup FROM SymantecCustomers " +
                         "JOIN EPACustomers ON EPACustomers.SymantecID = SymantecCustomers.Id " +
                         "JOIN CustomerPackageHistory ON SymantecCustomers.Id = CustomerPackageHistory.SymantecID AND Date = '" + DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd") + "' " +
                         "JOIN lookupPackages ON lookupPackages.PackageID = CustomerPackageHistory.PackageID";
            try
            {
                dtCustomer = MsSqlInterface.ExecuteSQLQueryMultiple( sql );
                return true;
            }
            catch( Exception ex )
            {
                Logger.LogException( "SelectCustomerData()", ex );
                return false;
            }
        }

        static private bool SelectEPACustomers()
        {

            Logger.Log( "SelectEPACustomers()", true );

            // Get all EPA customers
            string sql = "SELECT EPAID, SymantecID, SFAccount, IsReseller FROM EPACustomers";
            try
            {
                dtEPACustomer = MsSqlInterface.ExecuteSQLQueryMultiple( sql );
                return true;
            }
            catch( Exception ex )
            {
                Logger.LogException( "SelectEPACustomers()", ex );
                return false;
            }
        }

        #endregion


        #region MySQL data

        static private bool SelectLDAPCustomers()
        {

            Logger.Log( "SelectLDAPCustomers()", true );

            // Get all customers with packages, licences and package groups.
            string sql = "SELECT EPAID, ldapServerURL, ldapUsername, CONVERT(AES_DECRYPT(ldapPassword,'VC50wCzKCsiJm6nY39bqGjIDp9wzYnkGezaMtflXsuIUoMJHo9B95pJD8TyKIYq') USING utf8) AS ldapPassword, " +
                         "ldapBaseDN, ldapQuery, ldapAttributes, updateFrequency, stalePeriod, ldapTimeout FROM ldapCustomers";
            try
            {
                dtLDAPCustomer = MySqlInterface.ExecuteMySQLQueryMultiple( sql );
                return true;
            }
            catch( Exception ex )
            {
                Logger.LogException( "SelectLDAPCustomers()", ex );
                return false;
            }
        }

        #endregion

        static private void SelectAPIdata()
        {
            Console.WriteLine( "Loading Symantec API" );
            Logger.Log( "Loading Symantec API" );

            try
            {

                //                CreateAPICredentials();

                // Just echo a text
                //                Console.WriteLine( service.Echo( "Blåbærgrød" ) );

                SearchCustomersResponse response = null;
                foreach( DataRow item in dtCustomer.Rows )
                {
                    response = SymantecApi.SearchCustomer( ( int ) item[ 1 ], SearchType.CustomerName, true, StateType.Active );

                    foreach( var mainItem in response.SearchResults )
                    {
                        Console.WriteLine( string.Format( "{0}, {1} ", mainItem.Id, mainItem.Name ) );

                        // Read customer data
                        ReadCustomerResponse customerResponse = SymantecApi.ReadCustomer( mainItem.Id );

                        if ( customerResponse.Customer.HasDomains == true )
                        {

                            // Read domains
                            ReadDomainsResponse domainResponse = SymantecApi.ReadDomains( mainItem.Id );

                            foreach( var domain in domainResponse.Domains )
                            {
                                Console.WriteLine( string.Format( "     {0}, {1}, {2} ", domain.Id, domain.Name, domain.Services ) );
                            }
                        }

                    }
                }






/*
                // Master customer ID = 104761

                // Search for customer
                SearchCustomersResponse mainResponse = SymantecApi.SearchCustomer( 104761, SearchType.CustomerName, true, StateType.Active );

                // Loop through response result
                
                foreach( var mainItem in mainResponse.SearchResults )
                {
                    Console.WriteLine( string.Format( "{0}, {1} ", mainItem.Id, mainItem.Name ) );


                    SearchCustomersResponse customerResponse = SymantecApi.SearchCustomer( mainItem.Id, SearchType.CustomerName, true, StateType.Active );
                    foreach( var customerItem in customerResponse.SearchResults )
                    {
                         Console.WriteLine( string.Format( "     {0}, {1} ", customerItem.Id, customerItem.Name ) );

                        foreach( var domain in customerItem.Domains )
                        {
                            Console.WriteLine( string.Format( "          {0}, {1} ", domain.Id, domain.Name ) );
                        }
                    }
                }
*/





            }
            catch( Exception ex )
            {
                Console.WriteLine( "Error: " + ex.ToString() );
            }


            Console.Write( "Press any key............." );
            Console.ReadKey();



            //                string xx = service.GetAPIVersion();
            //            }




        }


        /// <summary>
        /// GenerateOutput()
        /// Generate JSON file for SalesForce and FuseMail
        /// </summary>
        static private void GenerateOutput()
        {

            Logger.Log( "Generating JSON", true );

            try
            {

                // Define filename for JSOn output file
                string SalesforceJsonFile = Path.GetDirectoryName( Assembly.GetExecutingAssembly().Location ) + @"\Output\" + masterFilenameSalesForce + "_" + datetimeStamp + ".txt";
                string FuseMailJsonFileBasic = Path.GetDirectoryName( Assembly.GetExecutingAssembly().Location ) + @"\Output\" + masterFilenameFuseMailBasic + "_" + datetimeStamp + ".txt";
                string FuseMailJsonFileAdv = Path.GetDirectoryName( Assembly.GetExecutingAssembly().Location ) + @"\Output\" + masterFilenameFuseMailAdv + "_" + datetimeStamp + ".txt";
                string FuseMailJsonFileBasicMS = Path.GetDirectoryName( Assembly.GetExecutingAssembly().Location ) + @"\Output\" + masterFilenameFuseMailBasicMS + "_" + datetimeStamp + ".txt";
                string FuseMailJsonFileAdvMS = Path.GetDirectoryName( Assembly.GetExecutingAssembly().Location ) + @"\Output\" + masterFilenameFuseMailAdvMS + "_" + datetimeStamp + ".txt";


                // Generate FuseMail Basic output
//                FuseMailBasic();

                // Generate FuseMail Advanced output
//                FuseMailAdvanced();

                FuseMailBasicMS();




                // Generate SalesForce output
//                Salesforce();

                try
                {
                    string jSon = string.Empty;

                    // Generate JSON output file ( SalesForce )
                    if( SalesForceJsonString.ToString() != string.Empty )
                    {
                        jSon = SalesForceJsonString.ToString();
                        using( var lfile = new StreamWriter( SalesforceJsonFile, false, Encoding.UTF8 ) )
                        {
                            lfile.Write( jSon );
                            lfile.Close();
                        }
                    }

                    // Generate JSON output file ( FuseMail Basic MS )
                    if( FuseMailBasicMSJsonString.ToString() != string.Empty )
                    {
                        jSon = FuseMailBasicMSJsonString.ToString();
                        using( var lfile = new StreamWriter( FuseMailJsonFileBasicMS, false, Encoding.UTF8 ) )
                        {
                            lfile.Write( jSon );
                            lfile.Close();
                        }
                    }

                    // Generate JSON output file ( FuseMail Basic )
                    if( FuseMailBasicJsonString.ToString() != string.Empty )
                    {
                        jSon = FuseMailBasicJsonString.ToString();
                        using( var lfile = new StreamWriter( FuseMailJsonFileBasic, false, Encoding.UTF8 ) )
                        {
                            lfile.Write( jSon );
                            lfile.Close();
                        }
                    }

                    // Generate JSON output file ( FuseMail Advanced )
                    if( FuseMailAdvJsonString.ToString() != string.Empty )
                    {
                        jSon = FuseMailAdvJsonString.ToString();
                        using( var lfile = new StreamWriter( FuseMailJsonFileAdv, false, Encoding.UTF8 ) )
                        {
                            lfile.Write( jSon );
                            lfile.Close();
                        }
                    }
                }
                catch( Exception ex )
                {
                    Logger.Log( "Error creating JSON file", true );
                    Logger.LogException( "GenerateOutput()", ex );
                }
            }
            catch( Exception ex )
            {
                Logger.LogException( "GenerateOutput()", ex );
            }
        }


        private static void SalesForceFindSpamService( DataRow EPAitem, Domain domainItem )
        {
            // Read spamservice for current domain
            ReadSpamServiceResponse ssResponse = SymantecApi.ReadSpamServiceForDomain( ( int ) EPAitem[ "SymantecID" ], ( int ) domainItem.Id );

            // Create spamservice for current domain
            DataRow newssRow = dtDomainSpamService.NewRow();
            newssRow[ "CustomerId" ] = ( int ) EPAitem[ "SymantecID" ];
            newssRow[ "DomainId" ] = ( int ) domainItem.Id;
            newssRow[ "UseDomainDefaultSettings" ] = ( bool ) ssResponse.SpamService.UseDomainDefaultSettings;
            newssRow[ "UseCustomIPAllowedList" ] = ( bool ) ssResponse.SpamService.UseCustomIPAllowedList;
            newssRow[ "UseCustomDomainAllowedList" ] = ( bool ) ssResponse.SpamService.UseCustomDomainAllowedList;
            newssRow[ "BulkMailAddress" ] = ssResponse.SpamService.BulkMailAddress.ToString();
            newssRow[ "SubjectTag" ] = ssResponse.SpamService.SubjectTag.ToString();
            newssRow[ "SubjectTagPosition" ] = ssResponse.SpamService.SubjectTagPosition.ToString();
            newssRow[ "HasQuarantine" ] = ( bool ) ssResponse.SpamService.HasQuarantine;

            newssRow[ "OrdbBlockedListAction" ] = ssResponse.SpamService.OrdbBlockedListAction.ToString();
            newssRow[ "RblBlockedListAction" ] = ssResponse.SpamService.RblBlockedListAction.ToString();
            newssRow[ "RssBlockedListAction" ] = ssResponse.SpamService.RssBlockedListAction.ToString();
            newssRow[ "DulBlockedListAction" ] = ssResponse.SpamService.DulBlockedListAction.ToString();
            newssRow[ "SigBlockedListAction" ] = ssResponse.SpamService.SigBlockedListAction.ToString();
            newssRow[ "CustomIPBlockedListAction" ] = ssResponse.SpamService.CustomIPBlockedListAction.ToString();
            newssRow[ "CustomDomainBlockedListAction" ] = ssResponse.SpamService.CustomDomainBlockedListAction.ToString();
            newssRow[ "HeuristicsBlockedListAction" ] = ssResponse.SpamService.HeuristicsBlockedListAction.ToString();
            newssRow[ "SpfBlockedListAction" ] = ssResponse.SpamService.SpfBlockedListAction.ToString();
            newssRow[ "RecipientCustomListCombination" ] = ssResponse.SpamService.RecipientCustomListCombination.ToString();
            newssRow[ "GroupCustomListCombination" ] = ssResponse.SpamService.GroupCustomListCombination.ToString();
            newssRow[ "RecipientCustomListMergePriority" ] = ssResponse.SpamService.RecipientCustomListMergePriority.ToString();
            newssRow[ "GroupCustomListMergePriority" ] = ssResponse.SpamService.GroupCustomListMergePriority.ToString();

            newssRow[ "HasNewsLetters" ] = ( bool ) ssResponse.SpamService.HasNewsLetters;
            newssRow[ "UseDMARC" ] = ( bool ) ssResponse.SpamService.UseDMARC;

            newssRow[ "NewslettersBlockedListAction" ] = ssResponse.SpamService.NewslettersBlockedListAction.ToString();
            newssRow[ "NewslettersSubjectTag" ] = ssResponse.SpamService.NewslettersSubjectTag.ToString();
            newssRow[ "NewslettersSubjectTagPosition" ] = ssResponse.SpamService.NewslettersSubjectTagPosition.ToString();

            dtDomainSpamService.Rows.Add( newssRow );
        }

        private static void SalesForceFindCustomerDomains( DataRow EPAitem, Domain domainItem )
        {
            // Create domains for current customer
            DataRow newdoRow = dtCustomerDomains.NewRow();
            newdoRow[ "CustomerId" ] = ( int ) EPAitem[ "SymantecID" ];
            newdoRow[ "Id" ] = ( int ) domainItem.Id;
            newdoRow[ "Name" ] = domainItem.Name.ToString();
            newdoRow[ "Services" ] = domainItem.Services.ToString();
            dtCustomerDomains.Rows.Add( newdoRow );
        }

        /// <summary>
        /// SalesforceHeader()
        /// </summary>
        private static void Salesforce()
        {
            SalesForceJsonString.Append( "{" );
            SalesForceJsonString.Append( "\"Export_System\":\"Version 1.0\"," );              // Default text
            SalesForceJsonString.Append( "\"Account detail\":" );
            SalesForceJsonString.Append( "[" );

            SalesForceCustomer();

            SalesForceJsonString.Append( "]" );
            SalesForceJsonString.Append( "}" );
        }

        private static void SalesForceCustomer()
        {
            int accountCount = 1;
            // Loop through all found EPA customers
            foreach( DataRow EPAitem in dtEPACustomer.Rows )
            {

                if( accountCount > 5 )
                {
                    Console.WriteLine( "Stopped after 10" );
                    break;
                }

                // Only use customers with a SalesForce reference
                if( EPAitem[ "SFAccount" ].ToString() != string.Empty )
                {
                    Logger.Log( "SymantecID = " + EPAitem[ "SymantecID" ].ToString(), true );

                    // Search current customer in MSSQL Customers
                    customerRow = dtCustomer.Select( "SymantecID = " + EPAitem[ "SymantecID" ].ToString() );

                    // Search customer
                    SearchCustomersResponse scResponse = SymantecApi.SearchCustomer( ( int ) EPAitem[ "SymantecID" ], SearchType.CustomerName, true, StateType.Active );

                    // Any information regarding current customer found
                    if( scResponse != null && scResponse.SearchResults.Count() > 0 )
                    {
                        var scItem = scResponse.SearchResults[ 0 ];

                        // Read current customer data
                        ReadCustomerResponse rcResponse = SymantecApi.ReadCustomer( scItem.Id );

                        // Customer services
                        customerServices = rcResponse.Customer.Services.ToString().Split( ',' );

                        // Clear DataTables
                        dtCustomerDomains.Clear();
                        dtDomainSpamService.Clear();

                        // Read domains for current customer
                        ReadDomainsResponse doResponse = SymantecApi.ReadDomains( ( int ) EPAitem[ "SymantecID" ] );

                        if( doResponse != null && doResponse.Domains.Count() > 0 )
                        {
                            foreach( var domainItem in doResponse.Domains )
                            {
                                // Add data to DataTables
                                SalesForceFindCustomerDomains( EPAitem, domainItem );
                                SalesForceFindSpamService( EPAitem, domainItem );
                            }
                        }

                        // Add comma (,) between customers
                        if( accountCount > 1 )
                        {
                            SalesForceJsonString.Append( "," );
                        }
                        SalesForceJsonString.Append( "{" );
                        SalesForceJsonString.Append( "\"SFId\": \"" + EPAitem[ "SFAccount" ].ToString() + "\"," );    // SF account id
                        SalesForceJsonString.Append( "\"Id\": \"" + EPAitem[ "SymantecID" ].ToString() + "\"," );     // Symantec account id
                        SalesForceJsonString.Append( "\"EPAId\": \"" + EPAitem[ "EPAID" ].ToString() + "\"," );       // EPA account id

                        FusemailDeployment( ref SalesForceJsonString, EPAitem, ref customerRow, ref scItem, ref rcResponse );
                        EmailDomains( ref SalesForceJsonString );
                        SymantecPackage( ref SalesForceJsonString );

                        SalesForceJsonString.Append( "}" );   // End Account / Customer
                        accountCount++;
                    }
                    else
                    {
                        Logger.Log( "     No data found. SearchCustomer()", true );
                    }
                }
            }

            Logger.Log( "Accounts generated: " + accountCount.ToString(), true );
        }

        /// <summary>
        /// FusemailDeployment()
        /// Generate "Fusemail Deployment" section of the JSON output
        /// </summary>
        /// <param name="JsonString"></param>
        /// <param name="EPAitem"></param>
        /// <param name="customerRow"></param>
        /// <param name="scItem"></param>
        /// <param name="rcResponse"></param>
        private static void FusemailDeployment( ref StringBuilder JsonString, DataRow EPAitem, ref DataRow[] customerRow, ref SearchCustomersResult scItem, ref ReadCustomerResponse rcResponse )
        {
            try
            {

                JsonString.Append( "\"Fusemail deployment\":" );
                JsonString.Append( "{" );

                // Source platform
                JsonString.Append( "\"Source_platform_account_ID__c\": \"" + EPAitem[ "SymantecID" ].ToString() + "\"," );
                JsonString.Append( "\"Source_platform_account_Type\": \"" + ( rcResponse.Customer.IsPartner.ToString() == "true" ? "Partner" : "Customer" ) + "\"," );

                // Reseller
                JsonString.Append( "\"Reseller__c\": \"" + EPAitem[ "SFAccount" ].ToString() + "\"," );

                // End Client
                JsonString.Append( "\"End_Client__c\": \"" + EPAitem[ "SymantecID" ].ToString() + "\"," );

                // Country
                JsonString.Append( "\"Country__c\": \"UK\"," );

                // Portal URL
                JsonString.Append( "\"Portal_URL__c\": null," );

                // No of licences
                // Find the largest value for Licenes in rows for current customer
                int maxLicences = 0;
                foreach( DataRow item in customerRow )
                {
                    int licences = ( int ) item[ "Licences" ];
                    maxLicences = ( licences > maxLicences ? licences : maxLicences );
                }
                JsonString.Append( "\"Licences__c\": " + maxLicences.ToString() + "," );

                JsonString.Append( "\"Migration_Cancelled__c\": false," );
                JsonString.Append( "\"Cancellation_Reason__c\": null," );

                // TODO
                //
                // Check Firewall status
                bool firewallStatus = false;
                JsonString.Append( "\"Firewall_access_verified__c\": " + firewallStatus.ToString().ToLower() + "," );

                JsonString.Append( "\"No_SPF_issues__c\": false," );

                // Generate tags for feature gaps
                FeatureGaps( ref JsonString, ref rcResponse );

                // TODO
                //
                // Find MX records for domain name
                //
                //DataRow[] resultMxRecords = dtCMIEmailDomains.Select( "Account_Id = '" + itemFusemail[ "Id" ].ToString() + "'" );
                //string mxRecords = string.Empty;
                //string domainname = string.Empty;
                //if( resultMxRecords.Count() > 0 )
                //{
                //    mxRecords += resultMxRecords[ 0 ][ "MX_Records" ].ToString();
                //    //domainname = resultMxRecords[ 0 ][ "Domain_name" ].ToString();
                //}


                // Outbound mail
                JsonString.Append( "\"Outbound_Mail__c\": \"" + ( scItem.OutboundMailServers.ToString() == string.Empty ? "Relaying Directly" : "Relaying through existing service" ) + "\"," );
                JsonString.Append( "\"Outbound_Mail_configured__c\": \"" + ( scItem.OutboundMailServers.ToString() == string.Empty ? "Nothing to configure" : "DNS needs updating" ) + "\"" );

                JsonString.Append( "}" );
            }
            catch ( Exception ex )
            {
                Logger.LogException( "FusemailDeployment()", ex );
            }
        }

        /// <summary>
        /// FeatureGaps()
        /// Generate "Feature gaps" section of the JSON output
        /// </summary>
        /// <param name="JsonString"></param>
        /// <param name="rcResponse"></param>
        private static void FeatureGaps( ref StringBuilder JsonString, ref ReadCustomerResponse rcResponse )
        {
            try
            {
                JsonString.Append( "\"Feature gaps\":" );

                if( rcResponse.Customer != null )
                {
                    JsonString.Append( "{" );

                    // Uses_2FA__c
                    JsonString.Append( "\"Uses_2FA__c\": " + rcResponse.Customer.HasTwoFactorAuthentication.ToString().ToLower() + "," );

                    // Compliance_Filter_Rules__c
                    JsonString.Append( "\"Compliance_Filter_Rules__c\": " + FindService( "DlpEmail" ).ToString().ToLower() + "," );

                    // Loop through all domains, spamservice and check for 'TagSubject'
                    string tagsubject = "tagsubject";
                    bool tabsubjectOk = false;
                    foreach( DataRow item in dtDomainSpamService.Rows )
                    {
                        if( item[ "SigBlockedListAction" ].ToString().ToLower() == tagsubject ||
                             item[ "HeuristicsBlockedListAction" ].ToString().ToLower() == tagsubject ||
                             item[ "OrdbBlockedListAction" ].ToString().ToLower() == tagsubject ||
                             item[ "RblBlockedListAction" ].ToString().ToLower() == tagsubject ||
                             item[ "RssBlockedListAction" ].ToString().ToLower() == tagsubject ||
                             item[ "DulBlockedListAction" ].ToString().ToLower() == tagsubject ||
                             item[ "CustomIPBlockedListAction" ].ToString().ToLower() == tagsubject ||
                             item[ "CustomDomainBlockedListAction" ].ToString().ToLower() == tagsubject )
                        {
                            tabsubjectOk = true;
                            break;
                        }
                    }

                    // Uses_spam_tagging__c
                    JsonString.Append( "\"Uses_spam_tagging__c\": " + tabsubjectOk.ToString().ToLower() );
                    JsonString.Append( "}," );
                }
                else
                {
                    JsonString.Append( "null" );
                }
            }
            catch( Exception ex )
            {
                Logger.LogException( "FeatureGaps()", ex );
            }
        }

        /// <summary>
        /// EmailDomains()
        /// Generate "Email Domains" section in the JSON file
        /// </summary>
        /// <param name="JsonString"></param>
        private static void EmailDomains( ref StringBuilder JsonString )
        {
            try
            {
                JsonString.Append( "," );
                JsonString.Append( "\"Email Domains\":" );

                if( dtCustomerDomains.Rows.Count > 0 )
                {
                    JsonString.Append( "[" );

                    int domainCount = 1;
                    foreach( DataRow domainItem in dtCustomerDomains.Rows )
                    {
                        // Add comma (,) between customers
                        if( domainCount > 1 )
                        {
                            JsonString.Append( "," );
                        }
                        JsonString.Append( "{" );

                        JsonString.Append( "\"Fusemail_Account_Type\": \"Customer\"," );
                        JsonString.Append( "\"Domain_Name__c\": \"" + domainItem[ "name" ].ToString() + "\"," );

                        // Search current customer in MSSQL Customers
                        DataRow[] domainSpamRow = dtDomainSpamService.Select( "DomainId = " + domainItem[ "Id" ].ToString() );

                        // Did we find any Domain Spam Record
                        if( domainSpamRow != null && domainSpamRow.Count() > 0 )
                        {
                            JsonString.Append( "\"SPF_Records__c\": \"" + domainSpamRow[ 0 ][ "SpfBlockedListAction" ].ToString() + "\"," );
                        }

                        JsonString.Append( "\"MX_Records_Changed__c\": \"?????\"" );


                        /*
                        // Find current Domain in dtSPFRecord
                        DataRow[] spfresult = dtSPFCheck.Select( "Hostname = '" + itemEmail[ "Domain_name" ].ToString() + "'" );
                        if( spfresult.Count() > 0 )
                        {
                            // Check SPF Status
                            switch( spfresult[ 0 ][ "Spf_status" ].ToString() )
                            {
                                case "OK":
                                    if( spfresult[ 0 ][ "Entries" ].ToString() != string.Empty )
                                    {
                                        JsonString.Append( "\"SPF_Records__c\": \"Checked - updated\"," );
                                    }
                                    else
                                    {
                                        JsonString.Append( "\"SPF_Records__c\": \"No SPF Record exist\"," );
                                    }
                                    break;

                                case "BAD":
                                    JsonString.Append( "\"SPF_Records__c\": \"Checked - need updating\"," );
                                    break;

                                default:
                                    JsonString.Append( "\"SPF_Records__c\": \"Not checked\"," );
                                    break;
                            }

                            // Check MX status ( Has MX record been changed to the new format )
                            switch( spfresult[ 0 ][ "Mx_status" ].ToString() )
                            {
                                case "OK":
                                    JsonString.Append( "\"MX_Records_Changed__c\": true," );
                                    break;

                                case "BAD":
                                default:
                                    JsonString.Append( "\"MX_Records_Changed__c\": false," );
                                    break;
                            }
                        }
                        else
                        {
                            // No SPF Records found for current Domain
                            JsonString.Append( "\"SPF_Records__c\": \"No SPF Record exist\"," );
                            JsonString.Append( "\"MX_Records_Changed__c\": false," );
                        }

                        JsonString.Append( "\"MX_Records__c\": \"" + HttpUtility.JavaScriptStringEncode( itemEmail[ "MX_Records" ].ToString() ) + "\"" );
        */

                        JsonString.Append( "}" );
                        domainCount++;
                    }

                    JsonString.Append( "]" );
                }
                else
                {
                    JsonString.Append( "null" );
                }
            }
            catch ( Exception ex )
            {
                Logger.LogException( "EmailDomains()", ex );
            }
        }

        /// <summary>
        /// SymantecPackage()
        /// Generate "Symantec Packages" section in the JSON file
        /// </summary>
        /// <param name="JsonString"></param>
        private static void SymantecPackage( ref StringBuilder JsonString )
        {
            try
            {
                JsonString.Append( "," );
                JsonString.Append( "\"Symantec Packages\":" );

                if( customerRow.Count() > 0 )
                {
                    JsonString.Append( "[" );

                    int customCount = 1;
                    foreach( DataRow item in customerRow )
                    {
                        // Add comma (,) between customers
                        if( customCount > 1 )
                        {
                            JsonString.Append( "," );
                        }

                        JsonString.Append( "{" );

                        JsonString.Append( "\"Package__c\": \"" + item[ "Packagename" ].ToString() + "\"," );
                        JsonString.Append( "\"Licences__c\": \"" + item[ "Licences" ].ToString() + "\"" );

                        JsonString.Append( "}" );
                        customCount++;
                    }

                    JsonString.Append( "]" );
                }
                else
                {
                    JsonString.Append( "null" );
                }
            }
            catch ( Exception ex )
            {
                Logger.LogException( "SymantecPackage()", ex );
            }
        }



        /// <summary>
        /// FuseMailBasicProperties()
        /// </summary>
        private static void FuseMailBasic()
        {
            FuseMailBasicJsonString.Append( "{" );
            FuseMailBasicJsonString.Append( "\"Export_System\":\"Version 1.0\"," );              // Default text
            FuseMailBasicJsonString.Append( "\"Properties\":" );
            FuseMailBasicJsonString.Append( "{" );

            FuseMailBasicCustomer();
            FuseMailBasicPackages();
            FuseMailBasicBillingcontacts();
            FuseMailBasicSite();
            FuseMailBasicRelayips();
            FuseMailBasicDomains();
            FuseMailBasicAdmin();

            FuseMailBasicJsonString.Append( "}" );
            FuseMailBasicJsonString.Append( "}" );
        }

        private static void FuseMailBasicCustomer()
        {
            FuseMailBasicJsonString.Append( "\"properties\":" );
            FuseMailBasicJsonString.Append( "{" );
            FuseMailBasicJsonString.Append( "\"address1\":\"?????\"," );
            FuseMailBasicJsonString.Append( "\"address2\":\"?????\"," );
            FuseMailBasicJsonString.Append( "\"city\":\"?????\"," );
            FuseMailBasicJsonString.Append( "\"country\":\"?????\"," );

            // customer_type
            //
            // "Direct Customer",
            // "Monthly Reseller",
            // "Yearly Reseller",
            // "Monthly Distributor"
            FuseMailBasicJsonString.Append( "\"customer_type\":\"?????\"," );         // Required

            FuseMailBasicJsonString.Append( "\"is_default_customer\":\"?????\"," );
            FuseMailBasicJsonString.Append( "\"customer_name\":\"?????\"," );
            FuseMailBasicJsonString.Append( "\"customer_notes\":\"?????\"," );
            FuseMailBasicJsonString.Append( "\"fax\":\"?????\"," );
            FuseMailBasicJsonString.Append( "\"internal_billing_id\":\"?????\"," );

            //invoice_method
            //
            // "Email",
            // "No Invoice",
            // "Postal Mail"
            FuseMailBasicJsonString.Append( "\"invoice_method\":\"?????\"," );        // Required

            // invoiceTerm
            // "Net/30",
            // "Pre-Paid",
            // "On Receipt",
            // "Net/25"
            FuseMailBasicJsonString.Append( "\"invoice_term\":\"????\"," );          // Required

            FuseMailBasicJsonString.Append( "\"minimum_charge\":\"0.0\"," );
            FuseMailBasicJsonString.Append( "\"next_billing_date\":\"????\"," );     // Format: 2015-01-13

            FuseMailBasicJsonString.Append( "\"parent_number\":\"?????\"," );        // Required

            // payment_frequency
            //
            // "Monthly",
            // "Annually"
            FuseMailBasicJsonString.Append( "\"payment_frequency\":\"?????\"," );    // Required

            // payment_methods
            //
            // "EFT / Cheque",
            // "Direct Debit"
            FuseMailBasicJsonString.Append( "\"payment_methods\":\"?????\"," );      // Required
            FuseMailBasicJsonString.Append( "\"postal_code\":\"?????\"," );

            // pricing_profile
            //
            // "CA - Direct",
            // "US - Direct",
            // "EU - Direct",
            // "UK - Direct",
            // "SE - Direct",
            // "DK - Direct",
            // "CA - Reseller",
            // "US - Reseller",
            // "EU - Reseller - Silver",
            // "UK - Reseller - Silver",
            // "SE - Reseller - Silver",
            // "DK - Reseller - Silver",
            // "EU - Reseller - Gold",
            // "UK - Reseller - Gold",
            // "SE - Reseller - Gold",
            // "DK - Reseller - Gold",
            // "EU - Reseller - Platinum",
            // "UK - Reseller - Platinum",
            // "SE - Reseller - Platinum",
            // "DK - Reseller - Platinum"
            FuseMailBasicJsonString.Append( "\"pricing_profile\":\"?????\"," );      // Required
            FuseMailBasicJsonString.Append( "\"province\":\"?????\"," );

            // region
            //
            // "CA",
            // "EU",
            // "UK",
            // "US",
            // "SE",
            // "DK"
            FuseMailBasicJsonString.Append( "\"region\":\"?????\"," );               // Required
            FuseMailBasicJsonString.Append( "\"sales_representative\":\"?????\"," ); // Required
            FuseMailBasicJsonString.Append( "\"timezone\":\"?????\"," );             // Required
            FuseMailBasicJsonString.Append( "\"trial_start_date\":\"?????\"," );     // Format: 2015-01-13
            FuseMailBasicJsonString.Append( "\"vat_number\":\"?????\"," );
            FuseMailBasicJsonString.Append( "\"work_phone\":\"?????\"," );
            FuseMailBasicJsonString.Append( "\"work_phone_ext\":\"?????\"," );

            FuseMailBasicJsonString.Append( "\"declared_billing\":\"?????\"," );
            FuseMailBasicJsonString.Append( "\"enable_alias\":\"?????\"," );

            // Allowed IP's
            FuseMailBasicAllowedIps();

            FuseMailBasicJsonString.Append( "\"ip_restriction_on_admin\":\"true/false\"," );
            FuseMailBasicJsonString.Append( "\"ip_restriction_on_user\":\"true/false\"," );

            // ip_restriction_type
            //
            // "AUTO_LOGIN", 
            // "CREDENTIALS", 
            // "NONE", 
            // "BOTH"
            FuseMailBasicJsonString.Append( "\"ip_restriction_type\":\"true/false\"," );
            FuseMailBasicJsonString.Append( "\"nfr_account\":\"true/false\"" );

            FuseMailBasicJsonString.Append( "}" );
            FuseMailBasicJsonString.Append( "}," );
        }

        private static void FuseMailBasicPackages()
        {
            FuseMailBasicJsonString.Append( "\"packages\":" );                       // Required
            FuseMailBasicJsonString.Append( "[" );
            FuseMailBasicJsonString.Append( "{" );

            // TODO
            // 
            // Foreach created package

            FuseMailBasicJsonString.Append( "\"items\":" );
            FuseMailBasicJsonString.Append( "{" );
            FuseMailBasicJsonString.Append( "\"properties\":" );
            FuseMailBasicJsonString.Append( "{" );

            // package_name
            //
            // "SecureSMART",
            // "SecureSMART Suite",
            // "ExchangeSMART",
            // "ExchangeSMART Suite",
            // "VaultSmart, 3 years",
            // "VaultSmart, unlimited",
            // "Image Analyzer",
            // "DNS Service",
            // "Extended Message Logs - 1 year",
            // "Extended Message Logs - 5 years",
            // "Extended Message Logs - 10 years",
            // "Extended Email Replay - 1 year",
            // "Extended Email Replay - 6 months",
            // "Privacy Smart"
            FuseMailBasicJsonString.Append( "\"package_name\":\"?????\"," );         // Required
            FuseMailBasicJsonString.Append( "\"licenses\":\"?????\"," );
            FuseMailBasicJsonString.Append( "\"start_date\":\"?????\"," );           // Format: 2015-01-13
            FuseMailBasicJsonString.Append( "\"discount\":\"?????\"" );
            FuseMailBasicJsonString.Append( "}" );
            FuseMailBasicJsonString.Append( "}" );
            FuseMailBasicJsonString.Append( "}]," );
        }

        private static void FuseMailBasicBillingcontacts()
        {
            FuseMailBasicJsonString.Append( "\"billingcontacts\":" );                // Required
            FuseMailBasicJsonString.Append( "[" );
            FuseMailBasicJsonString.Append( "{" );

            // TODO
            // 
            // Foreach created billingcontact

            FuseMailBasicJsonString.Append( "\"items\":" );
            FuseMailBasicJsonString.Append( "{" );
            FuseMailBasicJsonString.Append( "\"properties\":" );
            FuseMailBasicJsonString.Append( "{" );

            FuseMailBasicJsonString.Append( "\"first_name\":\"?????\"," );           // Required
            FuseMailBasicJsonString.Append( "\"last_name\":\"?????\"," );            // Required
            FuseMailBasicJsonString.Append( "\"title\":\"?????\"," );                
            FuseMailBasicJsonString.Append( "\"email\":\"?????\"," );                // Required
            FuseMailBasicJsonString.Append( "\"phone\":\"?????\"," );                // Required
            FuseMailBasicJsonString.Append( "\"phone_ext\":\"?????\"," );
            FuseMailBasicJsonString.Append( "\"cell\":\"?????\"," );
            FuseMailBasicJsonString.Append( "\"fax\":\"?????\"," );
            FuseMailBasicJsonString.Append( "\"fax_ext\":\"?????\"," );
            FuseMailBasicJsonString.Append( "\"pager\":\"?????\"," );

            // type
            //
            // "billing", 
            // "admin", 
            // "tech"
            FuseMailBasicJsonString.Append( "\"type\":\"?????\"," );                 // Required
            FuseMailBasicJsonString.Append( "\"is_primary\":\"?????\"" );

            FuseMailBasicJsonString.Append( "}" );
            FuseMailBasicJsonString.Append( "}" );
            FuseMailBasicJsonString.Append( "}]," );
        }

        private static void FuseMailBasicPermissions()
        {
            FuseMailBasicJsonString.Append( "\"permissions\":" );                // Required
            FuseMailBasicJsonString.Append( "[" );
            FuseMailBasicJsonString.Append( "{" );

            // TODO
            // 
            // Foreach created permission

            FuseMailBasicJsonString.Append( "\"items\":" );
            FuseMailBasicJsonString.Append( "{" );
            FuseMailBasicJsonString.Append( "\"properties\":" );
            FuseMailBasicJsonString.Append( "{" );

            // permission_target
            //
            // "View Spam Body",
            // "View Spam All",
            // "View Clean Body",
            // "View Clean All",
            // "Login as User"
            FuseMailBasicJsonString.Append( "\"permission_target\":\"?????\"," );    // Required

            FuseMailBasicJsonString.Append( "}," );
            FuseMailBasicJsonString.Append( "}" );
            FuseMailBasicJsonString.Append( "}]," );
        }

        private static void FuseMailBasicSite()
        {
            FuseMailBasicJsonString.Append( "\"site\":" );
            FuseMailBasicJsonString.Append( "{" );
            FuseMailBasicJsonString.Append( "\"properties\":" );
            FuseMailBasicJsonString.Append( "{" );
            FuseMailBasicJsonString.Append( "\"is_default\":\"?????\"," );   // Required
            FuseMailBasicJsonString.Append( "\"hosts\":" );                  // Required
            FuseMailBasicJsonString.Append( "[" );
            FuseMailBasicJsonString.Append( "{" );

            // TODO
            // 
            // Foreach created host
            FuseMailBasicJsonString.Append( "\"items\":" );
            FuseMailBasicJsonString.Append( "{" );
            FuseMailBasicJsonString.Append( "\"host\":\"?????\"," );         // Required

            // priority
            //
            // "minimum": 1,
            // "maximum": 9,
            FuseMailBasicJsonString.Append( "\"priority\":\"?????\"," );     // Required

            // port
            //
            // "minimum": 1,
            // "default": 25,
            FuseMailBasicJsonString.Append( "\"port\":\"?????\"" );
            FuseMailBasicJsonString.Append( "}" );
            FuseMailBasicJsonString.Append( "}]," );


            FuseMailBasicJsonString.Append( "\"site_name\":\"?????\"" );     // Required
            FuseMailBasicJsonString.Append( "}," );
        }

        private static void FuseMailBasicRelayips()
        {
            FuseMailBasicJsonString.Append( "\"relayips\":" );
            FuseMailBasicJsonString.Append( "{" );
            FuseMailBasicJsonString.Append( "\"items\":" );
            FuseMailBasicJsonString.Append( "[" );
            FuseMailBasicJsonString.Append( "{" );

            // TODO
            // 
            // Foreach created Relay IP's
           
            FuseMailBasicJsonString.Append( "\"properties\":" );
            FuseMailBasicJsonString.Append( "{" );
            FuseMailBasicJsonString.Append( "\"relay_ip\":\"?????\"," );   // Required
            FuseMailBasicJsonString.Append( "\"name\":\"?????\"" );
            FuseMailBasicJsonString.Append( "}" );



            FuseMailBasicJsonString.Append( "}]" );
            FuseMailBasicJsonString.Append( "}," );
        }

        private static void FuseMailBasicDomains()
        {
            FuseMailBasicJsonString.Append( "\"domains\":" );
            FuseMailBasicJsonString.Append( "{" );
            FuseMailBasicJsonString.Append( "\"items\":" );
            FuseMailBasicJsonString.Append( "[" );
            FuseMailBasicJsonString.Append( "{" );



            // TODO
            // 
            // Foreach related Domain

            FuseMailBasicJsonString.Append( "\"properties\":" );
            FuseMailBasicJsonString.Append( "{" );
            FuseMailBasicJsonString.Append( "\"Domain\":\"?????\"," );           // Required
            FuseMailBasicJsonString.Append( "\"is_primary\":\"?????\"," );       // Required
            FuseMailBasicJsonString.Append( "\"site_id\":\"?????\"," );

            // verify_users_by
            //
            // "SMTP",
            // "User list/LDAP",
            // "None"
            FuseMailBasicJsonString.Append( "\"verify_users_by\":\"?????\"" );  // Required






            FuseMailBasicJsonString.Append( "}" );
            FuseMailBasicJsonString.Append( "}]" );
            FuseMailBasicJsonString.Append( "}," );
        }

        private static void FuseMailBasicAdmin()
        {
            FuseMailBasicJsonString.Append( "\"admin\":" );
            FuseMailBasicJsonString.Append( "{" );
            FuseMailBasicJsonString.Append( "\"properties\":" );
            FuseMailBasicJsonString.Append( "{" );
            FuseMailBasicJsonString.Append( "\"first_name\":\"?????\"," );
            FuseMailBasicJsonString.Append( "\"last_name\":\"?????\"," );
            FuseMailBasicJsonString.Append( "\"password\":\"?????\"," );
            FuseMailBasicJsonString.Append( "\"primary_email\":\"?????\"" );
            FuseMailBasicJsonString.Append( "}" );
            FuseMailBasicJsonString.Append( "}" );
        }

        private static void FuseMailBasicAllowedIps()
        {
            FuseMailBasicJsonString.Append( "\"allowed_ips\":" );
            FuseMailBasicJsonString.Append( "{" );

            // TODO
            //
            // Foreach IP

            FuseMailBasicJsonString.Append( "\"items\":" );
            FuseMailBasicJsonString.Append( "{" );
            FuseMailBasicJsonString.Append( "\"properties\":" );
            FuseMailBasicJsonString.Append( "{" );

            // type
            //
            // "IP_V4", 
            // "IP_V4_RANGE", 
            // "IP_V4_WILDCARD", 
            // "CIDR", 
            // "IP_V6", 
            // "IP_V6_WILDCARD"
            FuseMailBasicJsonString.Append( "\"type\":\"????\"," );

            // restriction_on
            //
            // USER
            // ADMIN
            FuseMailBasicJsonString.Append( "\"restriction_on\":\"????\"," );
            FuseMailBasicJsonString.Append( "\"value\":\"????\"," );
            FuseMailBasicJsonString.Append( "\"name\":\"????\"" );

            FuseMailBasicJsonString.Append( "}" );
            FuseMailBasicJsonString.Append( "}" );
            FuseMailBasicJsonString.Append( "}," );
        }



        private static void FuseMailAdvanced()
        {
            FuseMailAdvJsonString.Append( "{" );
            FuseMailAdvJsonString.Append( "\"Export_System\":\"Version 1.0\"," );
            FuseMailAdvJsonString.Append( "\"Properties\":" );
            FuseMailAdvJsonString.Append( "{" );
            FuseMailAdvJsonString.Append( "\"name\":\"????\"," );                   // Required

            // mail_direction
            //
            // "inbound", 
            // "outbound"
            FuseMailAdvJsonString.Append( "\"mail_direction\":\"????\"," );         // Required
            FuseMailAdvJsonString.Append( "\"enabled\":\"????\"," );                // Required
            FuseMailAdvJsonString.Append( "\"notes\":\"????\"," );

            FuseMailAdvScope();
            FuseMailAdvRules();
            FuseMailAdvActions();

            FuseMailAdvJsonString.Append( "}" );
            FuseMailAdvJsonString.Append( "}" );
        }

        private static void FuseMailAdvScope()
        {
            FuseMailAdvJsonString.Append( "\"scope\":" );
            FuseMailAdvJsonString.Append( "{" );
            FuseMailAdvJsonString.Append( "\"properties\":" );
            FuseMailAdvJsonString.Append( "{" );

            // type
            //
            // "domain", 
            // "mailbox"
            FuseMailAdvJsonString.Append( "\"type\":\"????\"," );                   // Required

            // constraint
            //
            // all", 
            // "specified", 
            // "except"
            FuseMailAdvJsonString.Append( "\"constraint\":\"????\"," );             // Required

            FuseMailAdvJsonString.Append( "\"scope\":" );
            FuseMailAdvJsonString.Append( "[" );

            // TODO
            //
            // Foreach items found in scope
            //
            FuseMailAdvJsonString.Append( "{" );
            FuseMailAdvJsonString.Append( "\"items\":\"????\"" );
            FuseMailAdvJsonString.Append( "}" );


            FuseMailAdvJsonString.Append( "]" );
            FuseMailAdvJsonString.Append( "}" );
            FuseMailAdvJsonString.Append( "}," );
        }

        private static void FuseMailAdvRules()
        {
            FuseMailAdvJsonString.Append( "\"rules\":" );
            FuseMailAdvJsonString.Append( "{" );
            FuseMailAdvJsonString.Append( "\"properties\":" );
            FuseMailAdvJsonString.Append( "{" );

            // type
            //
            // "any", 
            // "all", 
            // "points_greater_than", 
            // "points_less_than", 
            // "always"
            FuseMailAdvJsonString.Append( "\"type\":\"????\"," );                   // Required
            FuseMailAdvJsonString.Append( "\"points\":\"????\"," );

            FuseMailAdvJsonString.Append( "\"entries\":" );
            FuseMailAdvJsonString.Append( "[" );



            // TODO
            //
            // Foreach items found in Rules
            //
            FuseMailAdvJsonString.Append( "{" );
            FuseMailAdvJsonString.Append( "\"items\":" );
            FuseMailAdvJsonString.Append( "{" );
            FuseMailAdvJsonString.Append( "\"properties\":" );
            FuseMailAdvJsonString.Append( "{" );
            FuseMailAdvJsonString.Append( "\"property\":\"????\"," );               // Required
            FuseMailAdvJsonString.Append( "\"comparator\":\"????\"," );             // Required
            FuseMailAdvJsonString.Append( "\"value\":\"????\"," );                  // Required

            // value_type
            //
            // "domain",
            // "mailbox",
            // "ip",
            // "text","
            // "list",
            // "int",
            // "string"
            FuseMailAdvJsonString.Append( "\"value_type\":\"????\"," );             // Required

            // occurrence_option
            //
            // "exactly", 
            // "at_least", 
            // "at_most"
            FuseMailAdvJsonString.Append( "\"occurrence_option\":\"????\"," );      // Required
            FuseMailAdvJsonString.Append( "\"occurrence\":\"????\"," );             // Required
            FuseMailAdvJsonString.Append( "\"assigned_points\":\"????\"" );         // Required

            FuseMailAdvJsonString.Append( "}" );
            FuseMailAdvJsonString.Append( "}" );
            FuseMailAdvJsonString.Append( "}" );



            FuseMailAdvJsonString.Append( "]" );
            FuseMailAdvJsonString.Append( "}" );
            FuseMailAdvJsonString.Append( "}," );
        }

        private static void FuseMailAdvActions()
        {
            FuseMailAdvJsonString.Append( "\"actions\":" );
            FuseMailAdvJsonString.Append( "[" );


            // TODO
            //
            // Foreach action
            //
            FuseMailAdvJsonString.Append( "{" );
            FuseMailAdvJsonString.Append( "\"items\":" );
            FuseMailAdvJsonString.Append( "{" );
            FuseMailAdvJsonString.Append( "\"properties\":" );
            FuseMailAdvJsonString.Append( "{" );

            FuseMailAdvJsonString.Append( "\"type\":\"????\"," );                   // Required
            FuseMailAdvJsonString.Append( "\"position\":\"????\"," );
            FuseMailAdvJsonString.Append( "\"contents\":\"????\"" );

            FuseMailAdvJsonString.Append( "}" );
            FuseMailAdvJsonString.Append( "}" );
            FuseMailAdvJsonString.Append( "}" );



            FuseMailAdvJsonString.Append( "]" );
        }



        private static void FuseMailBasicMS()
        {
            FuseMailBasicMSJsonString.Append( "{" );
            FuseMailBasicMSJsonString.Append( "\"Export_System\":\"Version 1.0\"," );              // Default text
            FuseMailBasicMSJsonString.Append( "\"Company\":\"????\"" );

            FuseMailBasicMSJsonString.Append( "{" );

            FuseMailBasicMSCustomer();

            FuseMailBasicMSJsonString.Append( "}" );
            FuseMailBasicMSJsonString.Append( "}" );
        }


        private static void FuseMailBasicMSCustomer()
        {
            int accountCount = 1;
            // Loop through all found EPA customers
            foreach( DataRow EPAitem in dtEPACustomer.Rows )
            {

                if( accountCount > 5 )
                {
                    Console.WriteLine( "Stopped after 10" );
                    break;
                }

                // Only use customers with a SalesForce reference
                if( EPAitem[ "SFAccount" ].ToString() != string.Empty )
                {
                    Logger.Log( "SymantecID = " + EPAitem[ "SymantecID" ].ToString(), true );

                    // Search current customer in MSSQL Customers
                    customerRow = dtCustomer.Select( "SymantecID = " + EPAitem[ "SymantecID" ].ToString() );

                    // Search customer
                    SearchCustomersResponse scResponse = SymantecApi.SearchCustomer( ( int ) EPAitem[ "SymantecID" ], SearchType.CustomerName, true, StateType.Active );

                    // Any information regarding current customer found
                    if( scResponse != null && scResponse.SearchResults.Count() > 0 )
                    {
                        SearchCustomersResult scItem = scResponse.SearchResults[ 0 ];

                        // Read current customer data
                        ReadCustomerResponse rcResponse = SymantecApi.ReadCustomer( scItem.Id );

                        FuseMailBasicMSJsonString.Append( "\"Company\":\"" +rcResponse.Customer.Name.ToString() + "\"" );
                        FuseMailBasicMSJsonString.Append( "\"details:\"" );
                        FuseMailBasicMSJsonString.Append( "{" );

                        FuseMailBasicMSAdmins( rcResponse );


                        FuseMailBasicMSJsonString.Append( "}" );   // End Company
                        accountCount++;
                    }
                    else
                    {
                        Logger.Log( "     No data found. SearchCustomer()", true );
                    }
                }
            }

            Logger.Log( "Accounts generated: " + accountCount.ToString(), true );
        }


        private static void FuseMailBasicMSAdmins( ReadCustomerResponse rcResponse )
        {
            FuseMailBasicJsonString.Append( "\"admins\":" );
            FuseMailBasicJsonString.Append( "[" );

            // Read Authorized users
            ReadAuthorizedUsersResponse auResponse = SymantecApi.ReadAuthorizedUsers( rcResponse.Customer.Id );

            int counter = 1;
            foreach( var item in auResponse.AuthorizedUsers )
            {
                // Add comma (,) between customers
                if( counter > 1 )
                {
                    FuseMailBasicMSJsonString.Append( "," );
                }

                string firstName = string.Empty;
                string lastName = string.Empty;
                SplitFullName( item.FullName.ToString() , ref firstName, ref lastName );

 

                FuseMailBasicMSJsonString.Append( "{" );
                FuseMailBasicMSJsonString.Append( "\"email\":\"" + item.EmailAddress.ToString() +"\"," );
                FuseMailBasicMSJsonString.Append( "\"firstName\"" + firstName + "\"," );
                FuseMailBasicMSJsonString.Append( "\"lastName\"" + lastName + "\"," );
                FuseMailBasicMSJsonString.Append( "\"password\":null" );
                FuseMailBasicMSJsonString.Append( "}" );

                counter++;
            }

            FuseMailBasicMSJsonString.Append( "]" );
            FuseMailBasicMSJsonString.Append( "}," );
        }






        private static void SplitFullName( string fullName, ref string firstName, ref string lastName )
        {
            // Split fullname into first- / lastname
            string[] splitname = fullName.Split( ' ' );
            int namesCount = splitname.Count() - 1;

            if( namesCount >= 1 )
            {
                for( int i = 0; i < namesCount; i++ )
                {
                    if( i > 0 )
                    {
                        firstName += " ";
                    }
                    firstName += splitname[ i ];
                }

                lastName = splitname[ namesCount ];
            }
            else
            {
                firstName = fullName;
            }
        }






        /// <summary>
        /// FindService()
        /// Find current service in the list of services
        /// </summary>
        /// <param name="service"></param>
        /// <returns></returns>
        private static bool FindService( string service )
        {
            try
            {
                string[] result = Array.FindAll( customerServices, s => s.Equals( service ) );
                return ( result.Length > 0 ? true : false );
            }
            catch ( Exception ex )
            {
                Logger.LogException( "FindService()", ex );
                return false;
            }
        }

        /// <summary>
        /// WriteToLog()
        /// </summary>
        /// <param name="logtext"></param>
        /// <param name="writeToConsole"></param>
        /// <param name="addTimeStamp"></param>
        /// <param name="writeBlankLine"></param>
        private static void WriteToLog( string logtext, bool writeToConsole = true, bool addTimeStamp = false, bool writeBlankLine = false )
        {
            Logger.Log( logtext );

            if( writeToConsole )
            {
                if( writeBlankLine )
                {
                    Console.WriteLine( "" );
                }

                if( addTimeStamp )
                {
                    Console.WriteLine( logtext + " " + DateTime.Now.ToLongTimeString() );
                }
                else
                {
                    Console.WriteLine( logtext );
                }
            }
        }

        /// <summary>
        /// ReadSetup()
        /// </summary>
        /// <returns></returns>
        static public bool ReadSetup()
        {
            // Read settings from App.config
            Console.WriteLine( "ReadSetup()" );
            try
            {
                // MS-SQL Connection
                connectionStringMSSQL = ConfigurationManager.AppSettings[ "MSSQLConnection" ];
                if( connectionStringMSSQL == string.Empty )
                {
                    string logtext = "Config parameter [ MSSQLConnection ] contains illigal value.";
                    Logger.Log( logtext );
                    Console.WriteLine( logtext );

                    return false;
                }

                // MYSQL Connection
                connectionStringMYSQL = ConfigurationManager.AppSettings[ "MYSQLConnection" ];
                if( connectionStringMYSQL == string.Empty )
                {
                    string logtext = "Config parameter [ MYSQLConnection ] contains illigal value.";
                    Logger.Log( logtext );
                    Console.WriteLine( logtext );

                    return false;
                }

                // API username and password
                username = ConfigurationManager.AppSettings[ "APIUsername" ];
                password = ConfigurationManager.AppSettings[ "APIPassword" ];

                // Times to retry establich connection to the SQL servers
                timesToRetryEstablishConnection = Convert.ToInt16( ConfigurationManager.AppSettings[ "TimesToRetryEstablishConnection" ] );

                // Overwrite logfile
                overwriteLogfile = Convert.ToBoolean( ConfigurationManager.AppSettings[ "OverwriteLogfile" ] );

            }
            catch( Exception ex )
            {
                Logger.LogException( "ReadSetup()", ex );
                return false;
            }

            return true;
        }

        /// <summary>
        /// CreateMYSQLCommection()
        /// </summary>
        /// <returns></returns>
        static public bool CreateMYSQLCommection()
        {
            Console.WriteLine( "CreateMYSQLCommection()" );

            MySqlInterface = new mySqlConnectionInterface();

            // Connect to MySQL 
            //
            // It happens that the VPN connection is down, so therefore there will be
            // tried to connect x no. of times.
            int retries = 0;
            bool returnValue = false;
            while( retries <= timesToRetryEstablishConnection )
            {
                try
                {
                    MySqlInterface.ConnectionString = connectionStringMYSQL;
                    MySqlInterface.OpenMySQLConnection();

                    returnValue = true;
                    break;

                }
                catch( Exception ex )
                {
                    Logger.LogException( "CreateMYSQLCommection()", ex );
                    Logger.Log( "Retrying : " + retries.ToString() + "/" + timesToRetryEstablishConnection.ToString() );
                    retries++;
                }
            }

            if ( returnValue )
            {
                Console.WriteLine( "CreateMYSQLCommection() - Connected" );
            }

            return returnValue;
        }

        /// <summary>
        /// CreateMSSQLCommection()
        /// </summary>
        /// <returns></returns>
        static public bool CreateMSSQLCommection()
        {
            Console.WriteLine( "CreateMSSQLCommection()" );

            MsSqlInterface = new msSqlConnectionInterface();

            // Connect to MSSQL 
            //
            // It happens that the VPN connection is down, so therefore there will be
            // tried to connect x no. of times.
            int retries = 0;
            bool returnValue = false;
            while( retries <= timesToRetryEstablishConnection )
            {
                try
                {
                    MsSqlInterface.ConnectionString = connectionStringMSSQL;
                    MsSqlInterface.OpenSQLConnection();

                    returnValue = true;
                    break;

                }
                catch( Exception ex )
                {
                    Logger.LogException( "CreateMSSQLCommection()", ex );
                    Logger.Log( "Retrying : " + retries.ToString() + "/" + timesToRetryEstablishConnection.ToString() );
                    retries++;
                }
            }

            if( returnValue )
            {
                Console.WriteLine( "CreateMSSQLCommection() - Connected" );
            }

            return returnValue;
        }

        /// <summary>
        /// ExitApplication()
        /// </summary>
        /// <param name="exitReason"></param>
        static private void ExitApplication( string exitReason )
        {
            string message = "Press any key to close this window.";

            Logger.Log( string.Format( "{0}: {1}", exitReason, application_name ) );

            Console.WriteLine( exitReason );
            Console.WriteLine( "" );
            Console.WriteLine( message );
            Console.ReadKey();

            // Exit
            Environment.Exit( 0 );

        }

        /// <summary> 
        /// API class for accessing Symantec data
        /// </summary>
        /// <param name="userAgent">The user agent.</param> 
        /// <returns></returns> 
        /// 
        internal static class SymantecApi
        {
            /// <summary> 
            /// ConfigServiceInstance()
            /// Configurate the customer’s API service. 
            /// </summary> 
            /// <param name="username">The username.</param> 
            /// <param name="password">The password.</param> 
            /// <returns></returns> 
            internal static ConfigService ConfigServiceInstance(string username, string password)
            {
                UsernameToken tokenConfigService = new UsernameToken(username, password, PasswordOption.SendPlainText);
                ConfigService configService = new ConfigService();
                configService.RequestSoapContext.Security.Tokens.Add(tokenConfigService);

                return configService;
            }

            /// <summary>
            /// SearchCustomer()
            /// Search for current customerID
            /// </summary>
            /// <param name="resellerId"></param>
            /// <param name="searchtype"></param>
            /// <param name="getnonprovisioneddomainsusername"></param>
            /// <param name="statetype"></param>
            /// <returns></returns>
            internal static SearchCustomersResponse SearchCustomer( int resellerId, SearchType searchtype, bool getnonprovisioneddomainsusername, StateType statetype = StateType.Active )
            {
                // Create a customer search request
                SearchCustomersRequest request = new SearchCustomersRequest();

                request.ResellerId = resellerId;
                request.Status = statetype;
                request.Type = searchtype;
                request.GetNonProvisionedDomains = getnonprovisioneddomainsusername;

                SearchCustomersResponse response = null;
                try
                {
                    // Execute customer search request
                    response = service.SearchCustomers( request );
                    return response;
                }
                catch( Exception ex )
                {
                    Logger.Log( "     Customer data not found", true );
                    Logger.LogException( "SearchCustomer()", ex );
                    return null;
                }
                
            }

            /// <summary>
            /// ReadCustomer()
            /// Read data for current customerID
            /// </summary>
            /// <param name="customerId"></param>
            /// <returns></returns>
            internal static ReadCustomerResponse ReadCustomer( int customerId )
            {
                // Create a customer request
                ReadCustomerRequest request = new ReadCustomerRequest();

                request.CustomerId = customerId;

                try
                {
                    // Execute customer request
                    ReadCustomerResponse response = service.ReadCustomer( request );
                    return response;
                }
                catch( Exception ex )
                {
                    Logger.LogException( "ReadCustomer()", ex );
                    return null;
                }
            }

            /// <summary>
            /// ReadDomains()
            /// Read Domains for current customerID
            /// </summary>
            /// <param name="customerId"></param>
            /// <returns></returns>
            internal static ReadDomainsResponse ReadDomains( int customerId )
            {
                // Create a domain request
                ReadDomainsRequest request = new ReadDomainsRequest();

                request.CustomerId = customerId;
                request.DomainId = -1;  // All domains
                request.IncludeAllDomainsForSameCustomer = true;

                try
                {
                    // Execute domain request
                    ReadDomainsResponse response = service.ReadDomains( request );
                    return response;
                }
                catch( Exception ex )
                {
                    Logger.LogException( "ReadDomainsResponse.ReadDomains()", ex );
                    return null;
                }
            }

            /// <summary>
            /// ReadDomainById()
            /// Read domain data for current domainId
            /// </summary>
            /// <param name="domainId"></param>
            /// <returns></returns>
            internal static ReadDomainsByDomainIdResponse ReadDomainById( int domainId )
            {
                // Create request
                ReadDomainsByDomainIdRequest request = new ReadDomainsByDomainIdRequest();

                request.DomainId = domainId;
                request.IncludeAllDomainsForSameCustomer = false;   // Only select data for current domainId

                try
                {
                    // Execute request
                    ReadDomainsByDomainIdResponse response = service.ReadDomainsByDomainId( request );
                    return response;
                }
                catch( Exception ex )
                {
                    Logger.LogException( "ReadDomainById()", ex );
                    return null;
                }
            }

            /// <summary>
            /// ReadSpamServiceForDomain()
            /// Read spamsettings for current domainId
            /// </summary>
            /// <param name="customerId"></param>
            /// <param name="domainId"></param>
            /// <returns></returns>
            internal static ReadSpamServiceResponse ReadSpamServiceForDomain( int customerId, int domainId )
            {
                // Create request
                ReadSpamServiceRequest request = new ReadSpamServiceRequest();

                request.CustomerId = customerId;
                request.DomainId = domainId;
                request.GetNewslettersConfiguration = true;

                try
                {
                    // Execute request
                    ReadSpamServiceResponse response = service.ReadSpamService( request );
                    return response;
                }
                catch( Exception ex )
                {
                    Logger.LogException( "ReadSpamServiceForDomain()", ex );
                    return null;
                }
            }

            /// <summary>
            /// ReadQuarantineConfigurationSettings()
            /// </summary>
            /// <param name="customerId"></param>
            /// <param name="domainId"></param>
            /// <returns></returns>
            internal static ReadQuarantineConfigurationSettingsResponse ReadQuarantineConfigurationSettings( int customerId, int domainId )
            {
                // Create request
                ReadQuarantineConfigurationSettingsRequest request = new ReadQuarantineConfigurationSettingsRequest();

                request.CustomerId = customerId;
                request.DomainId = domainId;    // -1 = all.

                try
                {
                    // Execute Quarantine Configuration Setting request
                    ReadQuarantineConfigurationSettingsResponse response = service.ReadQuarantineConfigurationSettings( request );
                    return response;
                }
                catch( Exception ex )
                {
                    Logger.LogException( "ReadQuarantineConfigurationSettings()", ex );
                    return null;
                }
            }

            /// <summary>
            /// ReadVirusService()
            /// </summary>
            /// <param name="customerId"></param>
            /// <param name="domainId"></param>
            /// <returns></returns>
            internal static ReadVirusServiceResponse ReadVirusService( int customerId, int domainId )
            {
                // Create request
                ReadVirusServiceRequest  request = new ReadVirusServiceRequest();

                request.CustomerId = customerId;
                request.DomainId = domainId;

                try
                {
                    // Execute request
                    ReadVirusServiceResponse response = service.ReadVirusService( request );
                    return response;
                }
                catch( Exception ex )
                {
                    Logger.LogException( "ReadVirusService()", ex );
                    return null;
                }
            }

            /// <summary>
            /// ReadOutboundEmailServers()
            /// </summary>
            /// <param name="customerId"></param>
            /// <returns></returns>
            internal static ReadOutboundEmailServersResponse ReadOutboundEmailServers( int customerId )
            {
                ReadOutboundEmailServersRequest request = new ReadOutboundEmailServersRequest();

                request.CustomerId = customerId;
                request.ReadActiveServers = true;
                request.ReadRequestedServers = true;

                try
                {
                    // Execute request
                    ReadOutboundEmailServersResponse response = service.ReadOutboundEmailServers( request );
                    return response;
                }
                catch( Exception ex )
                {
                    Logger.LogException( "ReadOutboundEmailServers()", ex );
                    return null;
                }
            }

            /// <summary>
            /// ReadInboundEmailServers()
            /// </summary>
            /// <param name="customerId"></param>
            /// <returns></returns>
            internal static ReadInboundEmailServersResponse ReadInboundEmailServers( int customerId )
            {
                ReadInboundEmailServersRequest request = new ReadInboundEmailServersRequest();

                request.CustomerId = customerId;
                request.DomainId = -1;
                request.ReadActiveServers = true;
                request.ReadRequestedServers = true;

                try
                {
                    // Execute request
                    ReadInboundEmailServersResponse response = service.ReadInboundEmailServers( request );
                    return response;
                }
                catch( Exception ex )
                {
                    Logger.LogException( "ReadInboundEmailServers()", ex );
                    return null;
                }
            }

            /// <summary>
            /// ReadServiceReportGroups()
            /// </summary>
            /// <param name="customerId"></param>
            /// <returns></returns>
            internal static ReadServiceReportGroupsResponse ReadServiceReportGroups( int customerId )
            {
                ReadServiceReportGroupsRequest request = new ReadServiceReportGroupsRequest();

                request.CustomerId = customerId;

                try
                {
                    // Execute request
                    ReadServiceReportGroupsResponse response = service.ReadServiceReportGroups( request );
                    return response;
                }
                catch( Exception ex )
                {
                    Logger.LogException( "ReadServiceReportGroups()", ex );
                    return null;
                }
            }

            /// <summary>
            /// ReadAuthorizedUser()
            /// </summary>
            /// <param name="customerId"></param>
            /// <param name="userId"></param>
            /// <returns></returns>
            internal static ReadAuthorizedUserResponse ReadAuthorizedUser( int customerId, int userId )
            {
                ReadAuthorizedUserRequest request = new ReadAuthorizedUserRequest();

                request.CustomerId = customerId;
                request.AuthorizedUserId = userId;   // 0 = All users

                try
                {
                    // Execute request
                    ReadAuthorizedUserResponse response = service.ReadAuthorizedUser( request );
                    return response;
                }
                catch( Exception ex )
                {
                    Logger.LogException( "ReadAuthorizedUser()", ex );
                    return null;
                }
            }

            /// <summary>
            /// ReadAuthorizedUsers()
            /// </summary>
            /// <param name="customerId"></param>
            /// <returns></returns>
            internal static ReadAuthorizedUsersResponse ReadAuthorizedUsers( int customerId )
            {
                ReadAuthorizedUsersRequest request = new ReadAuthorizedUsersRequest();

                request.CustomerId = customerId;
                try
                {
                    // Execute request
                    ReadAuthorizedUsersResponse response = service.ReadAuthorizedUsers( request );
                    return response;
                }
                catch( Exception ex )
                {
                    Logger.LogException( "ReadAuthorizedUsers()", ex );
                    return null;
                }
            }

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using System.Windows.Forms;
using System.Globalization;
using System.Reflection;

namespace Symantec_Reader
{
    /// <summary>
    /// Logger. This is a generic logging class that is used through out this app.
    /// </summary>
    internal class Logger
    {
//        private string logFilename = Path.Combine( Environment.GetFolderPath( Environment.SpecialFolder.ApplicationData ), @"ComendoMigration\Logs" );
        private string logFilename = Path.GetDirectoryName( Assembly.GetExecutingAssembly().Location ) + @"\Logs" ;

        static Object safe = new Object();

        /// <summary>
        /// Tries to place a file in the logs folder named 'yyyy-mm-dd.txt' for the current date. Shows a MessageBox with an error message and exception if not possible.
        /// </summary>
        private Logger()
        {
            try
            {
                if (!Directory.Exists(logFilename))
                    Directory.CreateDirectory(logFilename);

                DateTime dateValue = DateTime.Now;
                CultureInfo enUS = CultureInfo.CreateSpecificCulture("en-US");
                DateTimeFormatInfo dtfi = enUS.DateTimeFormat;
                dtfi.ShortDatePattern = "yyyy-MM-dd_HHmm";

                logFilename = Path.Combine( logFilename, dateValue.ToString( "d", enUS ) + ".txt" );

                // If overwriteLogfile = true, then delete log file if already exists
                if( File.Exists( logFilename ) && Program.overwriteLogfile )
                {
                    File.Delete( logFilename );
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show( "Comendo Migration could not initialize it's log file. Please contact your systemadministrator with the following info:" + Environment.NewLine +
                    ex.ToString(), "Comendo Migration - Critical error", MessageBoxButtons.OK, MessageBoxIcon.Error
                );
            }
        }


        public static string GetCurrentPath()
        {
            return log.logFilename;
        }

        private static Logger log = new Logger();
        /// <summary>
        /// Log. Logs a given message into the log file. Shows a MessageBox with an error message and an exception if not possible.
        /// </summary>
        /// <param name="msg">The message to be logged.</param>
        public static void Log( string msg, bool showConsole = false )
        {
            try
            {
                lock ( safe )
                {
                    using ( var lfile = new StreamWriter( log.logFilename, true ) )
                    {
//                        lfile.WriteLine( "{0} ------------------", DateTime.Now );
                        lfile.WriteLine( "{0} {1}", DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss.fff"), msg );
                        lfile.Close();
                    }

                    if ( showConsole )
                    {
                        Console.WriteLine( msg );
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Comendo Migration could not initialize it's log file (1). Please contact your systemadministrator with the following info:" + Environment.NewLine +
                    ex.ToString(), "Comendo Migration - Critical error", MessageBoxButtons.OK, MessageBoxIcon.Error
                );
            }
        }


        /// <summary>
        /// LogException. Logs a given message together with a given exception. Show a MessageBox with an error message and an exception if not possible.
        /// </summary>
        /// <param name="msg">The message to be logged.</param>
        /// <param name="ex">The exception to be logged.</param>
        public static void LogException(string msg, Exception ex, bool showConsole = false )
        {
            try
            {
                while (null != ex.InnerException)
                    ex = ex.InnerException;

                msg += Environment.NewLine + "--------------------" + Environment.NewLine + ex.ToString() + Environment.NewLine;

                Log(msg);

                if ( showConsole )
                {
                    Console.WriteLine( msg );
                }
            }
            catch (Exception exp)
            {
                MessageBox.Show("Comendo Migration could not initialize it's log file (2). Please contact your systemadministrator with the following info:" + Environment.NewLine +
                    exp.ToString(), "Comendo Migration - Critical error", MessageBoxButtons.OK, MessageBoxIcon.Error
                );
            }
        }
    }
}
